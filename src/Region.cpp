/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Region.h"
#include <algorithm>

namespace hawk {

/// Region implementation

bool Region::reduce_by(const Region& r)
{
  // left sub-range
  if (low < r.low && high <= r.high && high > r.low) {
    high = r.low;
    return true;
  }

  // right sub-range
  if (low <= r.high && high > r.high && low < r.high) {
    low = r.high;
    return true;
  }

  // sub-range
  if (low >= r.low && high <= r.high) {
    low = high = 0;
    return true;
  }

  // otherwise ranges are either disjoint
  // or `r' is a sub-range
  return false;
}

unsigned Region::reduce_by(const Region_list& rs)
{
  unsigned n = 0;
  for (const Region& r : rs) {
    if (reduce_by(r))
      n++;
  }

  return n;
}

bool Region::intersect_with(const Region& r)
{
  if (low >= r.low && high >= r.high && low < r.high) {
    high = r.high;
    return true;
  }

  if (low <= r.low && high <= r.high && high > r.low) {
    low = r.low;
    return true;
  }

  if (low <= r.low && high >= r.high) {
    *this = r;
    return true;
  }

  if (low >= r.low && high <= r.high) {
    return true;
  }

  low = 0;
  high = 0;
  return false;
}

std::vector<Region> Region::split_by(const Region_list& rs) const
{
  std::vector<Region> split;
  Region r = *this;
  uint64_t high = r.high; // save this for later

  for (const Region& i : rs) {
    if (i.is_subrange(r)) {
      r.high = i.low;
      split.push_back(r);
      r.low = i.high;
    }
  }

  if (split.empty())
    split.push_back(r);
  else
    split.push_back(Region{r.high, high});

  return split;
}

bool Region::overlaps_with(const Region r) const
{
  if (is_subrange(r) || r.is_subrange(*this))
    return true;

  // left or right sub-range
  if ((low < r.low && high <= r.high) || (low <= r.high && high > r.high))
    return true;

  return false;
}

/// Region_list implementation

void Region_list::insert(const Region& r)
{
  auto in = m_rs.insert(r);
  if (in.second)
    merge(in.first == m_rs.begin() ? in.first : std::prev(in.first));
}

void Region_list::clear() { m_rs.clear(); }

static bool at_end(Region_list& rs, Region_list::iterator end)
{
  return end == rs.end();
}

void Region_list::intersect(const Region& r)
{
  using namespace std;
  iterator low;
  iterator high;
  low = high = m_rs.begin();

  while (!at_end(*this, low)) {
    Region rr;
    bool intersects = false;

    do {
      rr = *high;
      ++high;

      intersects = rr.intersect_with(r);
    } while (!at_end(*this, high) && !intersects);

    m_rs.erase(low, high);

    if (intersects)
      m_rs.insert(rr);

    low = high;
  }
}

void Region_list::merge(iterator merge_from)
{
  while (merge_from != m_rs.end()) {
    Region r = *merge_from;

    auto merge_to = std::next(merge_from);
    while (merge_to != m_rs.end()) {
      if (r.high < merge_to->low)
        break;

      if (merge_to->high > r.high)
        r.high = merge_to->high;

      ++merge_to;
    }

    if (merge_to != std::next(merge_from)) {
      m_rs.erase(merge_from, merge_to);
      m_rs.insert(r);
    }

    merge_from = merge_to;
  }
}

} // namespace hawk
