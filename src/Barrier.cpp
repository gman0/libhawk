/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Barrier.h"

namespace hawk {

void Barrier::notify()
{
  std::lock_guard<std::mutex> lk{m_mtx};
  --m_count;
  m_cv.notify_all();
}

void Barrier::wait(int count)
{
  std::unique_lock<std::mutex> lk{m_mtx};
  m_count += count;
  m_cv.wait(lk, [&] { return m_count <= 0; });
}

} // namespace hawk
