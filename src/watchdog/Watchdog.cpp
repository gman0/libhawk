/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "watchdog/Watchdog.h"
#include "Filesystem.h"
#include "Path.h"
#include <algorithm>
#include <cassert>
#include <vector>

namespace hawk {

Watchdog::Watchdog(std::unique_ptr<Monitor>&& mon, Watchdog::Notify&& notify)
    : m_monitor{std::move(mon)}, m_notify{std::move(notify)}
{
  m_monitor->set_parent(*this);
  m_watchdog = Interruptible_thread([this] {
    {
      std::unique_lock<std::mutex> lk{m_mtx};
      m_cv.wait(lk, [&] { return !m_paths_to_add.empty(); });
    }

    for (;;) {
      hard_interruption_point();
      process_queued_paths();
      m_monitor->watch();
    }
  });
}

Watchdog::~Watchdog()
{
  m_watchdog.hard_interrupt();
  // Add a path in case m_watchdog is still waiting in m_cv.
  m_paths_to_add.insert(Path{});
  m_cv.notify_one();
}

void Watchdog::add_path(const Path& dir)
{
  assert(is_directory(dir));

  std::lock_guard<std::mutex> lk{m_mtx};
  m_paths_to_add.insert(dir);
  m_cv.notify_one();
}

void Watchdog::remove_path(const Path& dir)
{
  std::lock_guard<std::mutex> lk{m_mtx};
  m_paths_to_remove.insert(dir);
}

void Watchdog::_notify(Monitor::Event e, const Path& p) noexcept
{
  m_notify(e, p);
}

void Watchdog::process_queued_paths()
{
  std::lock_guard<std::mutex> lk{m_mtx};

  add_paths();
  m_paths_to_add.clear();

  remove_paths();
  m_paths_to_remove.clear();
}

void Watchdog::add_paths()
{
  if (m_paths_to_add.empty())
    return;

  std::vector<Path> diff;
  std::set_difference(m_paths_to_add.begin(), m_paths_to_add.end(),
                      m_paths_to_remove.begin(), m_paths_to_remove.end(),
                      std::back_inserter(diff));

  for (const Path& p : diff)
    m_monitor->add_path(p);
}

void Watchdog::remove_paths()
{
  if (m_paths_to_remove.empty())
    return;

  std::vector<Path> diff;
  std::set_difference(m_paths_to_add.begin(), m_paths_to_add.end(),
                      m_paths_to_remove.begin(), m_paths_to_remove.end(),
                      std::back_inserter(diff));

  for (const Path& p : diff)
    m_monitor->remove_path(p);
}

} // namespace hawk
