/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_TASKING_H
#define HAWK_TASKING_H

#include "Interruptible_thread.h"
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <functional>
#include <initializer_list>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <utility>

namespace hawk {

class Tasking {
public:
  // 0 - lowest priority
  using Priority = unsigned;

  struct Task {
    const Priority priority;

    explicit Task(Priority p) : priority{p} {}
    virtual ~Task() = default;
    virtual void run() = 0;
    virtual void commit() noexcept = 0;
  };

  struct Committing_task : Task {
    using Task::Task;
    void run() override final {}
  };

  using Exception_handler = std::function<void(std::exception_ptr) noexcept>;

  using Task_ptr = std::unique_ptr<Task>;
  using Committing_task_ptr = std::unique_ptr<Committing_task>;

private:
  struct Thread_context;

  struct Task_context {
    Task_ptr ptr;

    Task_context(Task_ptr&& task) : ptr{std::move(task)} {}
    bool operator()(Thread_context& ctx);
  };

  using Task_queue = std::queue<Task_context>;

  struct Thread_context {
    std::mutex m;
    std::condition_variable ready_cv;
    bool ready;

    std::condition_variable task_finished_cv;
    int finished_tasks;

    std::condition_variable tasking_started_cv;
    bool tasking_started;

    Priority priority;
    bool will_shutdown;
    Task_queue tasks;

    Thread_context()
        : ready{true}, finished_tasks{0}, tasking_started{false}, priority{0},
          will_shutdown{false}
    {
    }

    Thread_context(Thread_context&&) = delete;
    Thread_context& operator=(Thread_context&&) = delete;
  };

  using Thread_map = std::map<std::thread::id, Thread_context>;
  std::mutex m_tctx_mtx;
  Thread_map m_tctx;

  std::mutex m_eh_mtx;
  const Exception_handler exception_handler;
  const std::chrono::milliseconds m_detach_timeout;
  Interruptible_thread m_thread;

public:
  Tasking(std::chrono::milliseconds detach_timeout, Exception_handler f);
  ~Tasking();

  template <typename... TaskPtrs> bool run(Task_ptr&& first, TaskPtrs&&... ts)
  {
    Priority p = first->priority;
    Thread_context* ctx = switch_tasks(p);

    if (!ctx)
      return false;

    std::unique_lock<std::mutex> lk{ctx->m};
    ctx->ready_cv.wait(lk, [&] { return ctx->ready; });

    ctx->priority = p;
    empl(*ctx, std::move(first), std::forward<TaskPtrs>(ts)...);

    ctx->ready = false;
    ctx->ready_cv.notify_one();

    return true;
  }

  bool run_blocking(Committing_task_ptr&& ptr);
  void run_noint(Task_ptr&& ptr);
  void run_noint_blocking(Committing_task_ptr&& ptr);

private:
  void tbegin(Thread_context& ctx);
  void tend(Thread_context& ctx);
  void run_tasking();

  void empl(Thread_context&) {}

  template <typename X, typename... Xs>
  void empl(Thread_context& ctx, X&& x, Xs&&... xs)
  {
    ctx.tasks.emplace(std::forward<X>(x));
    empl(ctx, std::forward<Xs>(xs)...);
  }

  Thread_context* switch_tasks(Priority p);

  Thread_context& get_tctx();
  Thread_context& get_self_tctx();

  void create_tasking_thread();
};

} // namespace hawk

#endif // HAWK_TASKING_H
