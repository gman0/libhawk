/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_IO_TASK_H
#define HAWK_IO_TASK_H

#include "Filesystem.h"
#include "Interruptible_thread.h"
#include <chrono>
#include <mutex>
#include <memory>
#include <queue>

namespace hawk {

enum IO_flag { Deref_symlinks = 1, Update_symlinks = 2, Check_avail_space = 4 };
inline bool deref_symlinks(int flags) { return flags & Deref_symlinks; }
inline bool update_symlinks(int flags) { return flags & Update_symlinks; }
inline bool check_avail_space(int flags) { return flags & Check_avail_space; }

class IO_task {
public:
  enum class Status {
    not_started,
    preparing,
    pending,
    finished,
    failed,
    paused,
    canceled
  };

  struct Target {
    Path src;
    Path dst;

    Target() {}
    Target(const Path& s, const Path& d) : src{s}, dst{d} {}
  };

  using Target_queue = std::queue<Target>;

  struct File_progress {
    uintmax_t size;
    uintmax_t offset;
    uintmax_t rate;
    std::chrono::seconds eta;
  };

  struct Total_progress {
    Path src;
    Path dst;
    uintmax_t size;
    uintmax_t offset;
  };

  struct Context {
    uintmax_t total_offset; // Total offset for this target
    uintmax_t file_offset;  // Offset for current file
    Target t;
    Recursive_directory_iterator iter;
    std::chrono::steady_clock::time_point start;
    std::chrono::steady_clock::time_point last_file_progress;
    bool prepare_passed;

    Context(const Target& tt, IO_task* p)
        : total_offset{0}, file_offset{0}, t{tt}, prepare_passed{false},
          m_parent{p}
    {
    }

    void notify_task_progress(const IO_task::Total_progress& tp);
    void notify_file_progress(uintmax_t bytes_left, uintmax_t bytes_read);
    inline void increment_total_task_offset(uintmax_t size)
    {
      m_parent->m_total_offset += size;
    }

    const IO_task& parent() const { return *m_parent; }

  private:
    IO_task* m_parent;
  };

  friend class Context;

protected:
  Path m_dst;
  uintmax_t m_total_size;
  uintmax_t m_total_offset; // Total offset for all targets

  Target_queue m_ts;
  std::vector<Path> m_sources;

private:
  std::unique_ptr<Context> m_ctx;
  Status m_st;
  Interruptible_thread m_thread;

public:
  IO_task(const std::vector<Path>& srcs, const Path& dst);
  virtual ~IO_task() = default;

  uintmax_t total_size() const;
  uintmax_t total_offset() const;
  Status get_status() const;

  // (Re-)Starts the tasking. In case the tasking was paused or an error
  // occured prior to calling start(), the tasking will continue from where
  // it was interrupted before the event.
  void start();

  // Ends the tasking thread and sets the status to Status::paused.
  void pause();

  // Ends the tasking thread and sets the status to Status::canceled.
  // Starting the thread after being canceled is a no-op, thread won't start.
  void cancel();

protected:
  Context* context() const;

  virtual void prepare() = 0; // This shall throw Filesystem_error in case the
                              // task can't be executed (e.g. not enough disk
                              // space in destination)
  virtual void start_tasking() = 0;
  virtual void on_status_change(Status s) noexcept = 0;
  virtual void on_task_progress(const Total_progress& tp) noexcept = 0;
  virtual void on_file_progress(const File_progress& fp) noexcept = 0;

  // In case of an error (Filesystem_error exception), the tasking thread
  // will exit and Status::failed will be set. It's the user's responsibility
  // to deal with the error and call start() again.
  virtual void handle_error(const Filesystem_error& e, Target& t) noexcept = 0;
  virtual void handle_prepare_error(const Filesystem_error& e) noexcept = 0;

private:
  void set_status(Status s);
};

} // namespace hawk

#endif // HAWK_IO_TASK_H
