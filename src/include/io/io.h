/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_IO_H
#define HAWK_IO_H

#include "Filesystem.h"
#include "io/File.h"
#include "io/IO_task.h"

namespace hawk {

uintmax_t accumulate_size(const Path& dirpath);
uintmax_t accumulate_size_symlink(const Path& dirpath);

Path redirect_symlink_path(const Path& link_target, Path src, const Path& dst,
                           const Path& base_src, const Path& base_dst);

off64_t copy_file_block(File& dst, File& src);

void copy_file(IO_task::Context& ctx, const Path& src, const Path& dst,
               const Stat& st);

void copy_directory(IO_task::Context& ctx);
void copy_directory_symlink(IO_task::Context& ctx, IO_task::Target_queue& q);

} // namespace hawk

#endif // HAWK_IO_H
