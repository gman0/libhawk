/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_FILE_H
#define HAWK_FILE_H

#include "Path.h"
#include <fcntl.h>

namespace hawk {

class File {
private:
  int m_fd;

public:
  File(const Path& filepath, int flags, mode_t mode);
  ~File();

  int fd() const;

  off64_t read(char* buf, size_t sz);
  ssize_t write(char* buf, size_t sz);

  void seek(off64_t offset);
  off64_t tell() const;
};

} // namespace hawk

#endif // HAWK_FILE_H
