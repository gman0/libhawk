/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "io/IO_task.h"

#ifndef HAWK_IO_TASK_COPY_H
#define HAWK_IO_TASK_COPY_H

namespace hawk {

class IO_task_copy : public IO_task {
public:
  using IO_task::IO_task;

private:
  void start_tasking() override;
  void prepare() override;
};

} // namespace hawk

#endif // HAWK_IO_TASK_COPY_H
