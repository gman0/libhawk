/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_CACHE_H
#define HAWK_CACHE_H

#include "Path.h"
#include <cassert>
#include <cstddef>
#include <functional>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <queue>

namespace hawk {

using On_cache_free = std::function<void(const Path& dir) noexcept>;

// Inserting and retrieving elements to/from cache is thread-safe

template <typename Data, unsigned Max_age = 3> class Simple_cache {
public:
  using Data_ptr = std::shared_ptr<Data>;
  using Data_raw_ptr = Data*;

private:
  struct Entry {
    Data_ptr ptr;
    Path dir;
    int page;
    int age;

    Entry(const Path& d, int p) : dir{d}, page{p}, age{0} {}
    Entry(Data_ptr&& data, const Path& d, int p)
        : ptr{std::move(data)}, dir{d}, page{p}, age{0}
    {
    }
  };

  using List = std::list<Entry>;
  List m_acquired;
  std::mutex m_mtx; // Protects access to m_acquired

public:
  // Same as load(), but without free()ing
  Data_ptr find(const Path& dir, unsigned page)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    auto found = std::find_if(m_acquired.begin(), m_acquired.end(),
                              [&](const Entry& e) { return e.dir == dir; });
    if (found == m_acquired.end())
      return nullptr;

    if (found->page != page) {
      assert(found->ptr.use_count() == 1);
      free(found);

      return nullptr;
    }

    found->age = 0;

    return found->ptr;
  }

  Data_ptr load(const Path& dir, unsigned page)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    auto found = find_and_free(dir, page);
    if (!found.second)
      return nullptr;

    found.first->age = 0;

    if (found.first->page != page) {
      assert(found.first->ptr.use_count() == 1);
      free(found.first);

      return nullptr;
    }

    return found.first->ptr;
  }

  static Data_ptr acquire() { return std::make_shared<Data>(); }

  void store(Data_ptr ptr, const Path& dir, unsigned page)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    assert(std::find_if(m_acquired.begin(), m_acquired.end(),
                        [&](const Entry& ent) { return ent.dir == dir; }) ==
           m_acquired.end());

    m_acquired.emplace_back(std::move(ptr), dir, page);
  }

  bool discard(const Path& dir)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    auto found = std::find_if(m_acquired.begin(), m_acquired.end(),
                              [&](const Entry& ent) { return ent.dir == dir; });
    if (found != m_acquired.end()) {
      assert(found->ptr.use_count() == 1);
      free(found);

      return true;
    }

    return false;
  }

  void discard_all()
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    while (!m_acquired.empty()) {
      Entry& ent = m_acquired.front();
      assert(ent.ptr.use_count() == 1);
      m_acquired.pop_front();
    }
  }

private:
  typename List::iterator free(typename List::iterator it)
  {
    return m_acquired.erase(it);
  }

  bool will_free(Entry& ent)
  {
    if (ent.ptr.use_count() == 1) {
      if (++ent.age == Max_age)
        return true;
    }

    return false;
  }

  std::pair<typename List::iterator, bool> find_and_free(const Path& dir,
                                                         unsigned page)
  {
    auto found_it = m_acquired.end();
    bool found = false;

    for (auto it = m_acquired.begin(); it != m_acquired.end();) {
      if (it->dir == dir) {
        found_it = it++;
        found = true;
      }
      else {
        if (will_free(*it))
          it = free(it);
        else
          ++it;
      }
    }

    return {found_it, found};
  }
};

template <typename Data, typename Data_compare, unsigned Max_age = 3>
class Free_cache {
public:
  using Data_ptr = std::shared_ptr<Data>;
  using Data_raw_ptr = Data*;

private:
  const uint64_t m_pg_size;
  On_cache_free on_free;

  struct Entry {
    Data_ptr ptr;
    Path dir;
    int page;
    int age;

    Entry(Data_ptr& data, const Path& d, int p)
        : ptr{data}, dir{d}, page{p}, age{0}
    {
    }
  };

  using List = std::list<Entry>;
  using Free_queue =
      typename std::priority_queue<Data_ptr, std::vector<Data_ptr>,
                                   Data_compare>;

  List m_acquired;
  Free_queue m_free;
  std::mutex m_mtx; // Protects access to m_acquired and m_free

public:
  Free_cache(uint64_t page_size, On_cache_free f = nullptr)
      : m_pg_size{page_size}, on_free{std::move(f)}
  {
  }

  // Same as load(), but without free()ing
  Data_ptr find(const Path& dir, unsigned page)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    auto found = std::find_if(m_acquired.begin(), m_acquired.end(),
                              [&](const Entry& e) { return e.dir == dir; });
    if (found == m_acquired.end())
      return nullptr;

    if (found->page != page) {
      assert(found->ptr.use_count() == 1);
      free(found);

      return nullptr;
    }

    found->age = 0;

    return found->ptr;
  }

  Data_ptr load(const Path& dir, unsigned page)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    auto found = find_and_free(dir, page);
    if (!found.second)
      return nullptr;

    if (found.first->page != page) {
      assert(found.first->ptr.use_count() == 1);
      free(found.first);

      return nullptr;
    }

    found.first->age = 0;

    return found.first->ptr;
  }

  // Tries to acquire (pop) an entry from the free queue (m_free).
  // On success (m_free is non-empty), returns the popped entry,
  // otherwise a new one is created.
  Data_ptr acquire()
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    if (!m_free.empty()) {
      Data_ptr data = m_free.top();
      m_free.pop();
      return data;
    }

    return std::make_shared<Data>(m_pg_size);
  }

  void store(Data_ptr& ptr, const Path& dir, unsigned page)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    assert(std::find_if(m_acquired.begin(), m_acquired.end(),
                        [&](const Entry& ent) { return ent.dir == dir; }) ==
           m_acquired.end());

    m_acquired.emplace_back(ptr, dir, page);
  }

  bool discard(const Path& dir)
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    auto found = std::find_if(m_acquired.begin(), m_acquired.end(),
                              [&](const Entry& ent) { return ent.dir == dir; });
    if (found != m_acquired.end()) {
      assert(found->ptr.use_count() == 1);
      free(found);
    }

    return false;
  }

  void discard_all()
  {
    std::lock_guard<std::mutex> lk{m_mtx};

    while (!m_acquired.empty()) {
      Entry& ent = m_acquired.front();
      assert(ent.ptr.use_count() == 1);

      if (on_free)
        on_free(ent.dir);

      m_free.push(std::move(ent.ptr));
      m_acquired.pop_front();
    }
  }

private:
  typename List::iterator free(typename List::iterator it)
  {
    it->ptr->clear();
    if (on_free)
      on_free(it->dir);

    m_free.push(std::move(it->ptr));
    return m_acquired.erase(it);
  }

  // Tries to free an acquired entry if:
  // * its references count is 1, meaning it's only referenced by Acq_map
  // * its age is > Max_age
  // Freeing an entry means to move it from acquired entries (m_acquired)
  // to free queue (m_free), making it available for reuse upon later *_load
  // calls.
  // Age is bumped by 1 if the reference count is 1.
  bool will_free(Entry& ent)
  {
    if (ent.ptr.use_count() == 1) {
      if (++ent.age == Max_age)
        return true;
    }

    return false;
  }

  std::pair<typename List::iterator, bool> find_and_free(const Path& dir,
                                                         unsigned page)
  {
    auto found_it = m_acquired.end();
    bool found = false;

    for (auto it = m_acquired.begin(); it != m_acquired.end();) {
      if (it->dir == dir) {
        found_it = it++;
        found = true;
      }
      else {
        if (will_free(*it))
          it = free(it);
        else
          ++it;
      }
    }

    return {found_it, found};
  }
};

} // namespace hawk

#endif // HAWK_CACHE_H
