/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_REGION_H
#define HAWK_REGION_H

#include <cstddef>
#include <cstdint>
#include <set>
#include <vector>

namespace hawk {

class Region_list;

// A region is an interval of indices between [low, high).
struct Region {
  uint64_t low;
  uint64_t high;

  // Reduces this Region by `r'. This is a non-commutative operation.
  // Returns true if the Range was reduced.
  bool reduce_by(const Region& r);
  unsigned reduce_by(const Region_list& rs);

  bool intersect_with(const Region& r);

  // Splits this Region into multiple regions, i.e. generates a difference:
  //   this: *********************************
  //     rs:            *     ***    ****  ***
  // result: *********** *****   ****    **
  std::vector<Region> split_by(const Region_list& rs) const;

  // Returns true if this is a sub-range of `r'.
  // This is a non-commutative operation.
  inline bool is_subrange(const Region& r) const
  {
    return low >= r.low && high <= r.high;
  }

  inline bool is_inrange(uint64_t i) const { return i >= low && i < high; }

  inline uint64_t hash() const { return low + high * high; }

  bool overlaps_with(const Region r) const;

  inline bool is_zero() const { return low == high; }
};

namespace detail {
struct Region_compare {
  inline bool operator()(const Region& a, const Region& b) const
  {
    return a.low < b.low;
  }
};
} // namespace detail

class Region_list {
private:
  using Set = std::set<Region, detail::Region_compare>;
  Set m_rs;

public:
  using iterator = Set::iterator;
  using const_iterator = Set::const_iterator;
  using reverse_iterator = Set::reverse_iterator;
  using const_reverse_iterator = Set::const_reverse_iterator;

  void insert(const Region& r);
  void clear();

  // Leaves only the intersection of Region_list and `r':
  // Region_list: *****     ***** ** * *****
  //           r:                  ******
  //      result:                  * * **
  void intersect(const Region& r);

  iterator begin() const { return m_rs.begin(); }
  iterator end() const { return m_rs.end(); }
  const_iterator cbegin() const { return m_rs.cbegin(); }
  const_iterator cend() const { return m_rs.cend(); }

  reverse_iterator rbegin() const { return m_rs.rbegin(); }
  reverse_iterator rend() const { return m_rs.rend(); }
  const_reverse_iterator crbegin() const { return m_rs.crbegin(); }
  const_reverse_iterator crend() const { return m_rs.crend(); }

private:
  void merge(iterator merge_from);
};

} // namespace hawk

#endif // HAWK_REGION_H
