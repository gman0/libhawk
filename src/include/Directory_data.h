/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_DIRECTORY_DATA_H
#define HAWK_DIRECTORY_DATA_H

#include "Filesystem.h"
#include "Path.h"
#include <functional>
#include <vector>

namespace hawk {

namespace detail {

template <typename T> struct Uninitializing_allocator {
  using value_type = T;
  using pointer = value_type*;
  using const_pointer = const value_type*;
  using reference = value_type&;
  using const_reference = const value_type&;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;

  template <typename U> struct rebind {
    using other = Uninitializing_allocator<U>;
  };

  inline explicit Uninitializing_allocator() {}
  inline ~Uninitializing_allocator() {}
  inline explicit Uninitializing_allocator(const Uninitializing_allocator&) {}
  template <typename U>
  inline explicit Uninitializing_allocator(const Uninitializing_allocator<U>&)
  {
  }

  inline pointer address(reference r) { return &r; }
  inline const_pointer address(const_reference r) { return &r; }

  inline pointer allocate(size_type count,
                          typename std::allocator<void>::const_pointer = 0)
  {
    return reinterpret_cast<pointer>(malloc(count * sizeof(T)));
  }

  inline void deallocate(pointer p, size_type) { free(p); }

  inline size_type max_size() const
  {
    return std::numeric_limits<size_type>::max() / sizeof(T);
  }

  template <typename Ptr, typename... Args>
  inline void construct(Ptr*, Args&&...)
  {
  }
  inline void destruct(pointer p) {}

  inline bool operator==(Uninitializing_allocator const&) { return true; }
  inline bool operator!=(Uninitializing_allocator const& a)
  {
    return !operator==(a);
  }
};

} // namespace detail

struct Directory_data {
  // Filter used while loading directory entries.
  // Returns true if the entry shall be kept.
  using Filter = std::function<bool(const dirent64*) noexcept>;
  using Filters = std::vector<Filter>;

  std::vector<uint64_t> idx;
  // When querying for entry-type, don't use d_type directly,
  // intead use is_* overloads for dirent64 from Filesystem.h.
  std::vector<dirent64 /*, detail::Uninitializing_allocator<dirent64>*/> data;

  size_t sort_id;

  const uint64_t pg_size;
  Directory_iterator iter;

  explicit Directory_data(uint64_t page_size) : sort_id{0}, pg_size{page_size}
  {
  }

  Directory_data(const Directory_data&) = delete;
  Directory_data& operator=(const Directory_data&) = delete;

  void set_path(const Path& dir, int page, const Filters& fs);
  void clear();
  uint64_t size() const { return idx.size(); }
};

} // namespace hawk

#endif // HAWK_DIRECTORY_DATA_H
