/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_INTERRUPT_H
#define HAWK_INTERRUPT_H

#include <mutex>

namespace hawk {

void soft_interruption_point();
void hard_interruption_point();

class Interrupt_flag {
private:
  std::mutex m;
  bool flag;
  bool disabled;

public:
  Interrupt_flag() : flag{false}, disabled{false} {}

  void set();
  void clear();

  void disable();
  void enable();
  void enable_and_clear();

  friend void soft_interruption_point();
  friend void hard_interruption_point();
};

class Disable_interrupts {
private:
  Interrupt_flag& iflag;

public:
  Disable_interrupts(Interrupt_flag& interrupt_flag) : iflag{interrupt_flag}
  {
    iflag.disable();
  }
  ~Disable_interrupts() { iflag.enable(); }

  Disable_interrupts(const Disable_interrupts&) = delete;
  Disable_interrupts& operator=(const Disable_interrupts&) = delete;
};

class Disable_and_clear_interrupts {
private:
  Interrupt_flag& iflag;

public:
  Disable_and_clear_interrupts(Interrupt_flag& interrupt_flag)
      : iflag{interrupt_flag}
  {
    iflag.disable();
  }
  ~Disable_and_clear_interrupts() { iflag.enable_and_clear(); }

  Disable_and_clear_interrupts(const Disable_and_clear_interrupts&) = delete;
  Disable_and_clear_interrupts&
  operator=(const Disable_and_clear_interrupts&) = delete;
};

extern thread_local Interrupt_flag _soft_iflag;
extern thread_local Interrupt_flag _hard_iflag;

// These two classes are used as exception types.
struct Soft_thread_interrupt {
};
struct Hard_thread_interrupt {
};

} // namespace hawk

#endif // HAWK_INTERRUPT_H
