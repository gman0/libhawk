/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_BARRIER_H
#define HAWK_BARRIER_H

#include <condition_variable>
#include <mutex>

namespace hawk {

class Barrier {
private:
  int m_count;
  std::condition_variable m_cv;
  std::mutex m_mtx;

public:
  Barrier() : m_count{0} {}
  Barrier(const Barrier&) = delete;
  Barrier& operator=(const Barrier&) = delete;

  void notify();
  void wait(int count);
};

class Barrier_guard {
private:
  Barrier* b;

public:
  explicit Barrier_guard(Barrier& bb) : b{&bb} {}
  Barrier_guard(const Barrier_guard&) = delete;
  Barrier_guard& operator=(const Barrier_guard&) = delete;

  Barrier_guard(Barrier_guard&& bg) noexcept : b{bg.b} { bg.b = nullptr; }
  Barrier_guard& operator=(Barrier_guard&& bg) noexcept
  {
    if (this != &bg) {
      b = bg.b;
      bg.b = nullptr;
    }
    return *this;
  }

  ~Barrier_guard()
  {
    if (b)
      b->notify();
  }
};

} // namespace hawk

#endif // HAWK_BARRIER_H
