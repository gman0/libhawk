/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_INTERRUPTIBLE_THREAD_H
#define HAWK_INTERRUPTIBLE_THREAD_H

#include "Interrupt.h"
#include <future>
#include <thread>

namespace hawk {

class Interruptible_thread {
private:
  std::thread m_thread;
  Interrupt_flag* m_hard_iflag;
  Interrupt_flag* m_soft_iflag;

public:
  Interruptible_thread() : m_hard_iflag{nullptr}, m_soft_iflag{nullptr} {}

  template <typename Function, typename... Args>
  explicit Interruptible_thread(Function&& f, Args&&... args)
  {
    std::promise<Interrupt_flag*> hard;
    std::promise<Interrupt_flag*> soft;
    auto fhard = hard.get_future();
    auto fsoft = soft.get_future();

    m_thread = std::thread([&, f] {
      hard.set_value(&_hard_iflag);
      soft.set_value(&_soft_iflag);

      try {
        f(std::forward<Args>(args)...);
      }
      catch (const Hard_thread_interrupt&) {
      }
    });

    m_hard_iflag = fhard.get();
    m_soft_iflag = fsoft.get();
  }

  Interruptible_thread(Interruptible_thread&& ithread) noexcept;
  Interruptible_thread& operator=(Interruptible_thread&& ithread) noexcept;

  ~Interruptible_thread();

  Interruptible_thread(const Interruptible_thread&) = delete;
  Interruptible_thread& operator=(const Interruptible_thread&) = delete;

  std::thread::id get_id() const noexcept;

  void join();
  bool joinable() const;

  void detach();

  void hard_interrupt();
  void soft_interrupt();
};

} // namespace hawk

#endif // HAWK_INTERRUPTIBLE_THREAD_H
