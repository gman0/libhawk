/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_DIRECTORY_H
#define HAWK_DIRECTORY_H

#include "Directory_data.h"
#include "Filesystem.h"
#include "Path.h"
#include "Region.h"
#include <functional>
#include <memory>
#include <vector>

namespace hawk {

namespace detail {

struct Sort_idx_tag {
};
struct Static_idx_tag {
};

template <typename Tag> struct Directory_index {
  using tag_type = Tag;
  Directory_index(uint64_t index) : val{index} {}
  inline uint64_t value() const { return val; }
private:
  uint64_t val;
};

} // namespace detail

// n-th element in sorted order
using Sort_idx = detail::Directory_index<detail::Sort_idx_tag>;
// n-th element in static order (regardless of sorting)
using Static_idx = detail::Directory_index<detail::Static_idx_tag>;

class Directory {
public:
  using Compare = std::function<bool(Static_idx, Static_idx) noexcept>;
  using Apply = std::function<void(Static_idx) noexcept>;

private:
  std::shared_ptr<Directory_data> m_data;
  Path m_path;
  unsigned m_page;

public:
  Directory() {}

  Directory(const Path& dir, unsigned page,
            std::shared_ptr<Directory_data>& data)
      : m_data{data}, m_path{dir}, m_page{page}
  {
  }

  Directory(const Path& dir, unsigned page,
            std::shared_ptr<Directory_data>&& data)
      : m_data{std::move(data)}, m_path{dir}, m_page{page}
  {
  }

  // Checks if m_data is a non-null pointer.
  bool valid() const;
  // Resets m_data pointer.
  void release();

  // Returns loaded path.
  const Path& path() const;
  // Returns page number.
  unsigned page() const;

  // Checks if the page currently loaded is the last one, i.e.
  // if the internal Directory_iterator's at_end() returns true.
  bool is_last_page() const;

  // Methods below are all relative to the current page,
  // including the pos arguments.

  uint64_t size() const;

  // pos >= size() is undefined

  class Sorted {
    const Directory_data* d;

  public:
    explicit Sorted(const Directory_data* data) : d{data} {}
    inline const char* name(Sort_idx pos) const
    {
      return d->data[d->idx[pos.value()]].d_name;
    }
    inline unsigned char type(Sort_idx pos) const
    {
      return d->data[d->idx[pos.value()]].d_type;
    }
    inline ino64_t inode(Sort_idx pos) const
    {
      return d->data[d->idx[pos.value()]].d_ino;
    }
    inline Static_idx index(Sort_idx pos) const { return d->idx[pos.value()]; }
    inline uint64_t size() const { return d->data.size(); }
  };

  class Static {
    const Directory_data* d;

  public:
    explicit Static(const Directory_data* data) : d{data} {}
    inline const char* name(Static_idx pos) const
    {
      return d->data[pos.value()].d_name;
    }
    inline unsigned char type(Static_idx pos) const
    {
      return d->data[pos.value()].d_type;
    }
    inline ino64_t inode(Static_idx pos) const
    {
      return d->data[pos.value()].d_ino;
    }
    inline uint64_t size() const { return d->data.size(); }
  };

  Sorted sorted_access() const;
  Static static_access() const;

  // Applies function f() over a range of indices. In case of range overlap with
  // previous calls to apply(), the range is reduced to the non-overlaping set
  // of indices, i.e. single-pass guarantee.
  void apply(Region_list& rs, Sort_idx low, Sort_idx high, Apply f) const;

  // Applies function f() over `rs' sans [low, high).
  // Unlike apply(), `rs' is left unmodified. That means multiple calls to
  // inv_apply() with the same Region_list will result in the same calls of f().
  void inv_apply(const Region_list& rs, Sort_idx low, Sort_idx high,
                 Apply f) const;

  // Sorts current page using the supplied Compare function. Indices passed
  // to `cmp' are static regardless of the sorting.
  // `sort_id' is used to identify the Compare function, which is then saved
  // into its corresponding Directory_data. If the saved sort ID is equal
  // to `sort_id', sort() is a no-op because the data is already sorted by
  // this Compare function. If `sort_id' is 0, the ID is cmp's hash code
  // taken from typeid. It is in user's responsibility to set an appropriate ID
  // e.g. in case `cmp' is a plain pointer to function (since this would
  // generate the same hash for any such function).
  void sort(Compare cmp, size_t sort_id = 0);
};

} // namespace hawk

#endif // HAWK_DIRECTORY_H
