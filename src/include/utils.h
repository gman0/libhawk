#ifndef HAWK_UTILS_H
#define HAWK_UTILS_H

#include <algorithm>
#include <type_traits>

namespace hawk {

template <typename Container>
void swap_erase(Container& c, typename Container::iterator it)
{
  auto back_iter = std::prev(c.end());
  if (c.size() > 1 && it != back_iter)
    std::swap(*it, *back_iter);

  c.pop_back();
}

template <typename T, typename Self>
using enable_if_not_self =
    std::enable_if_t<!std::is_same<std::decay_t<T>, Self>::value>;

} // namespace hawk

#endif
