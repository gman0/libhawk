/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_PATH_H
#define HAWK_PATH_H

#include "utils.h"
#include <cstddef>
#include <string>
#include <utility>

namespace hawk {

class Path {
public:
  class iterator {
  private:
    std::string s;
    std::string::size_type n;

  public:
    iterator() : n{0} {}
    iterator(const Path& p);
    bool operator==(const iterator& i);
    bool operator!=(const iterator& i);
    Path operator*() const;
    iterator& operator++();
    iterator& operator--();
  };

  class reverse_iterator {
    std::string s;
    std::string::size_type n;

  public:
    reverse_iterator() : n{0} {}
    reverse_iterator(const Path& p);
    bool operator==(const reverse_iterator& i);
    bool operator!=(const reverse_iterator& i);
    Path operator*() const;
    reverse_iterator& operator++();
    reverse_iterator& operator--();
  };

private:
  mutable size_t m_hash;
  std::string m_path;

public:
  Path() : m_hash{0} {}

  template <typename Path_, typename = enable_if_not_self<Path_, Path>>
  Path(Path_&& p) : m_hash{0}, m_path{std::forward<Path_>(p)}
  {
    clear_trailing_separators();
  }

  template <typename Path_, typename = enable_if_not_self<Path_, Path>>
  Path& operator=(Path_&& p)
  {
    m_path = std::forward<Path_>(p);
    m_hash = 0;

    clear_trailing_separators();

    return *this;
  }

  Path(const char* p, std::string::size_type count)
      : m_hash{0}, m_path{p, count}
  {
    clear_trailing_separators();
  }

  Path(const std::string& str, std::string::size_type pos,
       std::string::size_type count)
      : m_hash{0}, m_path{str, pos, count}
  {
    clear_trailing_separators();
  }

  void clear();
  bool empty() const;

  std::string::size_type length() const;

  friend bool operator==(const Path& rhs, const Path& lhs);
  friend bool operator!=(const Path& rhs, const Path& lhs);

  // Checks for string equality instead of comparing Paths' hashes.
  // Use this method instead of operator== when only a single
  // comparison is needed as it is more efficient.
  bool string_equals(const Path& p) const;

  // Concatenate two paths.
  Path& operator/=(const Path& p);
  Path& operator/=(const char* p);

  bool operator<(const Path& rhs) const;

  const char* c_str() const;
  const std::string& string() const;

  Path parent_path() const;
  void set_parent_path();

  Path filename() const;
  void set_filename();

  // First non-root (/) node of the path
  Path front() const;
  void set_front();

  bool is_absolute() const;

  // Even though this method is marked as const it
  // may change the value of m_hash.
  size_t hash() const;

  bool is_parent_of(const Path& p) const;

  iterator begin() const { return iterator{*this}; }
  iterator end() const { return iterator(); }

  reverse_iterator rbegin() const { return reverse_iterator{*this}; }
  reverse_iterator rend() const { return reverse_iterator(); }

private:
  void append_separator_if_needed();
  void clear_trailing_separators();
};

// Concatenate two paths.
Path operator/(const Path& lhs, const Path& rhs);
Path operator/(const Path& lhs, const char* rhs);
Path operator/(const char* lhs, const Path& rhs);

// Compare two paths by comparing their hashes.
bool operator==(const Path& rhs, const Path& lhs);
bool operator!=(const Path& rhs, const Path& lhs);

bool is_absolute(const char* path);

} // namespace hawk

namespace std {
template <> struct hash<hawk::Path> {
  using argument_type = hawk::Path;
  using result_type = size_t;

  result_type operator()(const argument_type& p) const { return p.hash(); }
};
} // namespace std

#endif // HAWK_PATH_H
