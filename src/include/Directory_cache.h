/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_DIRECTORY_CACHE_H
#define HAWK_DIRECTORY_CACHE_H

#include "Cache.h"
#include "Directory.h"
#include "Directory_data.h"

namespace hawk {

namespace detail {
using Directory_ptr = std::shared_ptr<Directory_data>;
struct Directory_ptr_cmp {
  bool operator()(const Directory_ptr& a, const Directory_ptr& b) const
  {
    return a->size() > b->size();
  }
};
} // namespace detail

using Directory_cache = Free_cache<Directory_data, detail::Directory_ptr_cmp>;

} // namespace hawk

#endif // HAWK_DIRECTORY_CACHE_H
