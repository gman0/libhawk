/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HAWK_STAT_CACHE_H
#define HAWK_STAT_CACHE_H

#include "Cache.h"
#include "Filesystem.h"
#include "Region.h"
#include <map>
#include <mutex>

namespace hawk {

struct Stat_cache_data {
  Region_list rs;
  std::map<uint64_t, Stat> map;

  // In case of multi-threaded access
  std::mutex m;

  void clear()
  {
    rs.clear();
    map.clear();
  }
};

using Stat_cache = Simple_cache<Stat_cache_data>;

} // namespace hawk

#endif // HAWK_STAT_CACHE_H
