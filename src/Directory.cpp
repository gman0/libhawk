/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Directory.h"
#include "Interrupt.h"
#include <algorithm>
#include <cassert>

namespace hawk {

void Directory::release() { m_data.reset(); }

const Path& Directory::path() const { return m_path; }

bool Directory::valid() const { return m_data.get(); }

unsigned Directory::page() const { return m_page; }

bool Directory::is_last_page() const { return m_data->iter.at_end(); }

uint64_t Directory::size() const { return m_data->size(); }

Directory::Sorted Directory::sorted_access() const
{
  return Sorted{m_data.get()};
}

Directory::Static Directory::static_access() const
{
  return Static{m_data.get()};
}

namespace {

struct Target_apply {
  Directory_data* d;
  Region_list& rs;
  Region target;

  Target_apply(Directory_data* data, Region_list& reglist, const Region& r)
      : d{data}, rs{reglist}, target{r}
  {
    target.reduce_by(rs);
  }

  void apply(Directory::Apply&& f)
  {
    if (target.is_zero())
      return;

    // In case we get interrupted, we still get the correct
    // number of elements that were apply()'d.
    uint64_t high = target.high;

    for (const Region& r : target.split_by(rs)) {
      target.high = r.low;

      while (target.high < high) {
        f(d->idx[target.high++]);
        soft_interruption_point();
      }
    }
  }

  // Regardless of how many/little (even in case of interrupt) elements
  // were processed, we still need to create a new region (single-pass
  // guarantee).
  ~Target_apply() { rs.insert(target); }
};

} // unnamed-namespace

void Directory::apply(Region_list& rs, Sort_idx low, Sort_idx high,
                      Directory::Apply f) const
{
  Target_apply apl{m_data.get(), rs, Region{low.value(), high.value()}};
  apl.apply(std::move(f));
}

void Directory::inv_apply(const Region_list& rs, Sort_idx low, Sort_idx high,
                          Directory::Apply f) const
{
  Region ir{low.value(), high.value()};
  for (Region target : rs) {
    target.reduce_by(ir);
    if (target.is_zero())
      continue;

    for (; target.low < target.high; target.low++) {
      f(m_data->idx[target.low]);
      soft_interruption_point();
    }
  }
}

void Directory::sort(Directory::Compare cmp, size_t sort_id)
{
  if (sort_id == 0)
    sort_id = cmp.target_type().hash_code();

  if (sort_id == m_data->sort_id)
    return; // Entries are already sorted using this Compare function.

  // Reset sort_id as we might get interrupted.
  // sort_id == 0 means incomplete sort
  m_data->sort_id = 0;

  std::sort(m_data->idx.begin(), m_data->idx.end(),
            [&](Static_idx a, Static_idx b) {
              soft_interruption_point();
              return cmp(a, b);
            });

  m_data->sort_id = sort_id;
}

} // namespace hawk
