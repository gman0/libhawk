/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Path.h"
#include <cassert>
#include <cstdlib>
#include <functional>

namespace hawk {

void Path::clear()
{
  m_path.clear();
  m_hash = 0;
}

bool Path::empty() const { return m_path.empty(); }

std::string::size_type Path::length() const { return m_path.length(); }

bool operator==(const Path& rhs, const Path& lhs)
{
  return const_cast<Path&>(rhs).hash() == const_cast<Path&>(lhs).hash();
}

bool operator!=(const Path& rhs, const Path& lhs)
{
  return const_cast<Path&>(rhs).hash() != const_cast<Path&>(lhs).hash();
}

bool Path::string_equals(const Path& p) const { return m_path == p.m_path; }

Path& Path::operator/=(const Path& p)
{
  m_hash = 0;
  if (p.empty())
    return *this;

  if (this != &p) {
    if (p.m_path.front() != '/')
      append_separator_if_needed();

    m_path += p.m_path;
  }
  else {
    Path rhs{p};
    if (rhs.m_path.front() != '/')
      append_separator_if_needed();

    m_path += rhs.m_path;
  }

  m_hash = 0;

  clear_trailing_separators();

  return *this;
}

Path& Path::operator/=(const char* p)
{
  if (*p != '/')
    append_separator_if_needed();

  m_path += p;
  m_hash = 0;

  clear_trailing_separators();

  return *this;
}

bool Path::operator<(const Path& rhs) const { return m_path < rhs.m_path; }

Path operator/(const Path& lhs, const Path& rhs) { return Path(lhs) /= rhs; }

const char* Path::c_str() const { return m_path.c_str(); }

const std::string& Path::string() const { return m_path; }

Path Path::parent_path() const
{
  auto pos = m_path.rfind('/');

  if (pos == std::string::npos)
    return Path{};
  if (pos == 0)
    return (m_path[1] == '\0') ? Path{} : Path{"/"};

  return Path{m_path.c_str(), pos};
}

void Path::set_parent_path()
{
  const std::string::size_type pos = m_path.rfind('/');

  if (pos == std::string::npos)
    m_path.clear();
  else {
    if (pos == 0) {
      if (m_path[1] == '\0')
        m_path.clear();
      else
        m_path = "/";
    }
    else
      m_path = m_path.substr(0, pos);
  }

  m_hash = 0;
}

Path Path::filename() const
{
  auto pos = m_path.rfind('/');

  if (pos == std::string::npos || pos == 1)
    return *this;

  return Path{m_path.c_str() + pos + 1, m_path.length() - pos - 1};
}

void Path::set_filename()
{
  auto pos = m_path.rfind('/');
  m_hash = 0;

  if (pos == std::string::npos || pos == 1)
    return;

  m_path = m_path.substr(pos + 1);
}

Path Path::front() const
{
  std::string::size_type n;
  if (is_absolute())
    n = m_path.find('/', 1);
  else
    n = m_path.find('/');

  return m_path.substr(0, n);
}

void Path::set_front()
{
  m_path = std::move(front().m_path);
  m_hash = 0;
}

bool Path::is_absolute() const { return !m_path.empty() && m_path[0] == '/'; }

size_t Path::hash() const
{
  if (m_hash == 0)
    m_hash = std::hash<std::string>()(m_path);

  return m_hash;
}

bool Path::is_parent_of(const Path& p) const
{
  if (empty() || p.empty())
    return false;

  if (length() > p.length())
    return false;

  return std::mismatch(m_path.cbegin(), m_path.cend(), p.m_path.cbegin())
             .first == m_path.cend();
}

void Path::append_separator_if_needed()
{
  if (!m_path.empty() && m_path.back() != '/')
    m_path += '/';
}

void Path::clear_trailing_separators()
{
  if (length() > 1 && m_path.back() == '/') {
    auto pos = m_path.find_last_not_of('/');
    if (pos != std::string::npos)
      m_path = m_path.substr(0, pos + 1);
  }
}

bool is_absolute(const char* path) { return *path == '/'; }

Path operator/(const Path& lhs, const char* rhs) { return Path{lhs} /= rhs; }

Path operator/(const char* lhs, const Path& rhs) { return Path{lhs} /= rhs; }

Path::iterator::iterator(const Path& p) : s{p.m_path}
{
  n = s.find('/');
  if (p.is_absolute())
    operator++();
}

bool Path::iterator::operator==(const Path::iterator& i)
{
  return n == i.n && s == i.s;
}

bool Path::iterator::operator!=(const Path::iterator& i)
{
  return !operator==(i);
}

Path Path::iterator::operator*() const { return Path(s.substr(0, n)); }

Path::iterator& Path::iterator::operator++()
{
  if (n == std::string::npos) {
    s.clear();
    n = 0;
  }
  else
    n = s.find('/', n + 1);

  return *this;
}

Path::iterator& Path::iterator::operator--()
{
  n = s.rfind('/', n - 1);
  return *this;
}

Path::reverse_iterator::reverse_iterator(const Path& p) : s{p.m_path}
{
  n = s.rfind('/');
}

bool Path::reverse_iterator::operator==(const Path::reverse_iterator& i)
{
  return n == i.n && s == i.s;
}

bool Path::reverse_iterator::operator!=(const Path::reverse_iterator& i)
{
  return !operator==(i);
}

Path Path::reverse_iterator::operator*() const
{
  int nn = (n > 0) ? n + 1 : n;
  return Path(s.substr(nn));
}

Path::reverse_iterator& Path::reverse_iterator::operator++()
{
  if (n == 0) {
    s.clear();
    n = 0;
  }
  else
    n = s.rfind('/', n - 1);

  return *this;
}

Path::reverse_iterator& Path::reverse_iterator::operator--()
{
  n = s.find('/', n + 1);
  return *this;
}

} // namespace hawk
