/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Tasking.h"

namespace hawk {

namespace {

using Barrier = std::promise<void>;
class Barrier_guard {
private:
  Barrier& b;

public:
  Barrier_guard(Barrier& barrier) : b{barrier} {}
  ~Barrier_guard() { b.set_value(); }
};

struct Blocking_committing_task : Tasking::Committing_task {
  Tasking::Committing_task* parent;
  Barrier& b;

  Blocking_committing_task(Tasking::Committing_task* par, Barrier& br)
      : Tasking::Committing_task(par->priority), parent{par}, b{br}
  {
  }

  void commit() noexcept override
  {
    Barrier_guard bg{b};
    parent->commit();
  }
};

} // unnamed-namespace

Tasking::Tasking(std::chrono::milliseconds detach_timeout, Exception_handler f)
    : exception_handler{std::move(f)}, m_detach_timeout{detach_timeout}
{
  create_tasking_thread();
}

Tasking::~Tasking()
{
  Thread_context& ctx = get_tctx();

  {
    std::unique_lock<std::mutex> lk{ctx.m};

    ctx.ready_cv.wait(lk, [&] { return ctx.ready; });
    ctx.will_shutdown = true;
    ctx.ready_cv.notify_one();
  }

  m_thread.join();
}

bool Tasking::run_blocking(Committing_task_ptr&& ptr)
{
  Barrier b;
  auto f = b.get_future();
  bool r = run(std::make_unique<Blocking_committing_task>(ptr.get(), b));

  if (r)
    f.get();

  return r;
}

void Tasking::run_noint(Tasking::Task_ptr&& ptr)
{
  Thread_context& ctx = get_tctx();
  std::unique_lock<std::mutex> lk{ctx.m};
  ctx.ready_cv.wait(lk, [&] { return ctx.ready; });

  ctx.priority = ptr->priority;
  ctx.ready = false;
  empl(ctx, std::move(ptr));

  ctx.ready_cv.notify_one();
}

void Tasking::run_noint_blocking(Committing_task_ptr&& ptr)
{
  Barrier b;
  auto f = b.get_future();
  run_noint(std::make_unique<Blocking_committing_task>(ptr.get(), b));
  f.get();
}

void Tasking::tbegin(Tasking::Thread_context& ctx)
{
  std::unique_lock<std::mutex> lk{ctx.m};

  ctx.tasking_started = true;
  ctx.tasking_started_cv.notify_one();

  ctx.ready_cv.wait(lk, [&] { return !ctx.ready || ctx.will_shutdown; });
  _soft_iflag.clear();
  ctx.finished_tasks = 0;

  ctx.ready_cv.notify_one();
}

void Tasking::tend(Tasking::Thread_context& ctx)
{
  std::unique_lock<std::mutex> lk{ctx.m};

  ctx.ready = true;
  ctx.tasking_started = false;
  ctx.priority = 0;
  ctx.tasks = Task_queue{};

  ctx.ready_cv.notify_one();
}

void Tasking::run_tasking()
{
  Thread_context& ctx = get_self_tctx();

  for (;;) {
    tbegin(ctx);

    if (ctx.will_shutdown)
      break;

    try {
      while (!ctx.tasks.empty()) {
        Task_context& task = ctx.tasks.front();

        {
          std::lock_guard<std::mutex> lk{ctx.m};
          ctx.priority = task.ptr->priority;
        }

        if (!task(ctx))
          break;

        ctx.tasks.pop();
      }
    }
    catch (Hard_thread_interrupt) {
      break;
    }
    catch (...) {
      std::lock_guard<std::mutex> lk{m_eh_mtx};
      exception_handler(std::current_exception());
    }

    tend(ctx);
  }

  std::lock_guard<std::mutex> lk{m_tctx_mtx};
  m_tctx.erase(std::this_thread::get_id());
}

Tasking::Thread_context* Tasking::switch_tasks(Priority p)
{
  Thread_context& ctx = get_tctx();
  std::unique_lock<std::mutex> lk{ctx.m};
  if (p < ctx.priority)
    return nullptr;

  if (!ctx.tasks.empty()) {
    // If we didn't wait for tasking_started, tbegin()'s clear_flag might
    // get called right after soft_interrupt(), which would cancel the interrupt
    ctx.tasking_started_cv.wait(lk, [&] { return ctx.tasking_started; });
    m_thread.soft_interrupt();

    int finished_tasks = ctx.finished_tasks;
    if (!ctx.task_finished_cv.wait_for(lk, m_detach_timeout, [&] {
          return finished_tasks < ctx.finished_tasks || ctx.tasks.empty();
        })) {
      ctx.will_shutdown = true;
      m_thread.detach();

      create_tasking_thread();
      return &get_tctx();
    }
  }

  return &ctx;
}

bool Tasking::Task_context::operator()(Tasking::Thread_context& ctx)
{
  bool interrupted = false;
  try {
    ptr->run();
  }
  catch (Soft_thread_interrupt) {
    interrupted = true;
  }

  {
    std::lock_guard<std::mutex> lk{ctx.m};

    ++ctx.finished_tasks;
    ctx.task_finished_cv.notify_one();

    if (ctx.will_shutdown)
      return false;
  }

  Disable_and_clear_interrupts dis{_soft_iflag};
  ptr->commit();

  return !interrupted;
}

Tasking::Thread_context& Tasking::get_tctx()
{
  std::lock_guard<std::mutex> lk{m_tctx_mtx};
  return m_tctx[m_thread.get_id()];
}

Tasking::Thread_context& Tasking::get_self_tctx()
{
  std::lock_guard<std::mutex> lk{m_tctx_mtx};
  return m_tctx[std::this_thread::get_id()];
}

void Tasking::create_tasking_thread()
{
  m_thread = Interruptible_thread([this] { run_tasking(); });
  std::lock_guard<std::mutex> lk{m_tctx_mtx};
  m_tctx.emplace(std::piecewise_construct, std::make_tuple(m_thread.get_id()),
                 std::make_tuple());
}

} // namespace hawk
