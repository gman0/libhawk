/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "io/io.h"
#include "Interrupt.h"
#include <algorithm>
#include <numeric>
#include <vector>

namespace hawk {

uintmax_t accumulate_size(const Path& dirpath)
{
  return std::accumulate(Recursive_directory_iterator{dirpath},
                         Recursive_directory_iterator(), uintmax_t(),
                         [&](uintmax_t val, const Path& p) -> uintmax_t {
                           hard_interruption_point();
                           const Stat st = symlink_status(dirpath / p);

                           if (is_regular_file(st))
                             return file_size(st) + val;

                           return val;
                         });
}

uintmax_t accumulate_size_symlink(const Path& dirpath)
{
  uintmax_t sz = 0;
  std::vector<Path> ps{dirpath};

  while (!ps.empty()) {
    Path pp = ps.back();
    ps.pop_back();
    sz += std::accumulate(Recursive_directory_iterator{pp},
                          Recursive_directory_iterator(), uintmax_t(),
                          [&](uintmax_t val, const Path& p) -> uintmax_t {
                            hard_interruption_point();
                            const Stat st = symlink_status(pp / p);

                            if (is_regular_file(st))
                              return file_size(st) + val;

                            if (is_symlink(st)) {
                              Path deref = canonical(pp / p, "/");
                              if (!dirpath.is_parent_of(deref)) {
                                const Stat deref_st = status(deref);
                                if (is_directory(deref_st))
                                  ps.push_back(std::move(deref));
                                else if (is_regular_file(deref_st))
                                  return file_size(deref_st) + val;
                              }
                            }

                            return 0 + val;
                          });
  }

  return sz;
}

off64_t copy_file_block(File& dst, File& src)
{
  constexpr unsigned buf_sz = 8192;
  char buf[buf_sz];

  const off64_t read_sz = src.read(buf, buf_sz);
  if (read_sz == 0)
    return 0;

  if (read_sz == -1)
    throw Filesystem_error{errno};

  if (dst.write(buf, read_sz) != read_sz)
    throw Filesystem_error{errno};

  return read_sz;
}

void copy_file(IO_task::Context& ctx, const Path& src, const Path& dst,
               const Stat& st)
{
  if (exists(dst) && ctx.file_offset == 0)
    throw Filesystem_error{dst, EEXIST};

  uintmax_t bytes_left = file_size(st);

  File src_file{src, O_RDONLY, 0440};
  File dst_file{dst, O_WRONLY | O_CREAT, 0666};

  if (bytes_left == 0)
    return; // An empty file.

  if (ctx.file_offset > 0) {
    src_file.seek(ctx.file_offset);
    dst_file.seek(ctx.file_offset);
    bytes_left -= ctx.file_offset;
  }

  int err =
      posix_fadvise64(src_file.fd(), ctx.file_offset, 0, POSIX_FADV_SEQUENTIAL);
  if (err)
    throw Filesystem_error{src, err};

  // Set timers & counters.

  ctx.start = std::chrono::steady_clock::now();
  uintmax_t bytes_read = 0; // Bytes read in this context.

  // Commence copying!

  constexpr unsigned buf_sz = 8192;
  char buf[buf_sz];

  for (;;) {
    hard_interruption_point();

    const off64_t read_sz = src_file.read(buf, buf_sz);
    if (read_sz == 0)
      break;

    if (read_sz == -1)
      throw Filesystem_error{src, errno};

    if (dst_file.write(buf, read_sz) != read_sz)
      throw Filesystem_error{dst, errno};

    bytes_read += read_sz;
    ctx.file_offset += read_sz;
    ctx.notify_file_progress(bytes_left, bytes_read);
  }
}

void copy_directory(IO_task::Context& ctx)
{
  while (!ctx.iter.at_end()) {
    hard_interruption_point();

    const Path p = *ctx.iter;
    const Path src = ctx.t.src / p;
    const Path dst = ctx.t.dst / p;
    const Stat st = symlink_status(src);

    ctx.notify_task_progress(IO_task::Total_progress{
        src, dst, ctx.parent().total_size(), ctx.parent().total_offset()});

    if (is_directory(st)) {
      int err;
      create_directory(dst, err);
      if (err && err != EEXIST) // Ignore existing directory.
        throw Filesystem_error{dst, err};
    }
    else if (is_regular_file(st)) {
      copy_file(ctx, src, dst, st);
      ctx.file_offset =
          0; // The file was successfuly copied, without interrupts
             // => reset the offset.
      ctx.total_offset += file_size(st);
      ctx.increment_total_task_offset(file_size(st));
    }
    else if (is_symlink(st)) {
      create_symlink(redirect_symlink_path(read_symlink(src), src, dst,
                                           ctx.t.src, ctx.t.dst),
                     dst);
    }

    // Increment the iterator.

    if (is_symlink(st))
      ctx.iter.orthogonal_increment();
    else
      ++ctx.iter;
  }
}

void copy_directory_symlink(IO_task::Context& ctx, IO_task::Target_queue& q)
{
  while (!ctx.iter.at_end()) {
    hard_interruption_point();

    const Path p = *ctx.iter;
    const Path src = ctx.t.src / p;
    const Path dst = ctx.t.dst / p;
    const Stat st = symlink_status(src);

    ctx.notify_task_progress(IO_task::Total_progress{
        src, dst, ctx.parent().total_size(), ctx.parent().total_offset()});

    if (is_directory(st)) {
      int err;
      create_directory(dst, err);
      // Ignore existing directory.
      if (err && err != EEXIST)
        throw Filesystem_error{dst, err};
    }
    else if (is_regular_file(st)) {
      copy_file(ctx, src, dst, st);
      ctx.file_offset =
          0; // The file was successfuly copied, without interrupts
             // => reset the offset.
      ctx.total_offset += file_size(st);
      ctx.increment_total_task_offset(file_size(st));
    }

    if (is_symlink(st)) {
      Path deref = canonical(src, "/");
      if (!deref.is_parent_of(src))
        q.push({deref, ctx.t.dst});

      ctx.iter.orthogonal_increment();
    }
    else
      ++ctx.iter;
  }
}

Path redirect_symlink_path(const Path& link_target, Path src, const Path& dst,
                           const Path& base_src, const Path& base_dst)
{
  src.set_parent_path();
  if (src.is_parent_of(link_target))
    return dst.parent_path() / (link_target.c_str() + src.length());
  else if (link_target.is_parent_of(src))
    return base_dst / (link_target.c_str() + base_src.length());

  return link_target;
}

} // namespace hawk
