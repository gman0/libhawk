#include "io/IO_task_move.h"
#include "io/io.h"

namespace hawk {

void IO_task_move::start_tasking()
{
  const Stat st_dst = status(m_dst);

  while (!m_ts.empty()) {
    const Target& t = m_ts.front();
    const Stat st = symlink_status(context()->t.src);

    context()->notify_task_progress(
        Total_progress{t.src, m_dst, m_total_size, m_total_offset});

    if (is_same_device(st, st_dst)) {
      if (exists(t.dst))
        throw Filesystem_error{t.dst, EEXIST};

      rename_move(t.src, t.dst);
    }
    else {
      if (is_regular_file(st)) {
        copy_file(*context(), t.src, t.dst, st);
        remove_file(t.src);
        m_total_offset += context()->file_offset;
      }
      else if (is_directory(st)) {
        if (context()->total_offset == 0) {
          context()->iter = Recursive_directory_iterator{t.src};
          create_directory(t.dst);
        }

        copy_directory(*context());
        remove_directory(t.src);
      }
      else if (is_symlink(st)) {
        create_symlink(t.dst, read_symlink(t.src));
        remove_file(t.src);
      }
    }

    context()->t = t;
    context()->total_offset = 0;
    context()->file_offset = 0;
    m_ts.pop();
  }
}

void IO_task_move::prepare()
{
  uint64_t total = 0;
  const Stat st_dst = status(m_dst);

  for (const Path& s : m_sources) {
    hard_interruption_point();

    const Stat st = symlink_status(s);
    if (!is_same_device(st, st_dst)) {
      if (is_directory(st))
        total += accumulate_size(s);
      else
        total += file_size(st);
    }
  }

  m_total_size = total;

  if (total > space(m_dst).available)
    throw Filesystem_error{m_dst, ENOSPC};
}

} // namespace hawk
