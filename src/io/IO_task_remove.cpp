#include "io/IO_task_remove.h"
#include "io/io.h"

namespace hawk {

void IO_task_remove::start_tasking()
{
  while (!m_ts.empty()) {
    const Target& t = m_ts.front();
    Stat st = symlink_status(t.src);

    context()->notify_task_progress(Total_progress{t.src, m_dst, 0, 0});

    if (is_directory(st))
      remove_recursively(t.src);
    else
      remove_file(t.src);

    context()->t = t;
    m_ts.pop();
  }
}

void IO_task_remove::prepare() {}

} // namespace hawk
