#include "io/File.h"
#include "Filesystem.h"
#include <fcntl.h>
#include <unistd.h>

namespace hawk {

File::File(const Path& filepath, int flags, mode_t mode)
{
  m_fd = open64(filepath.c_str(), flags, mode);
  if (m_fd == -1)
    throw Filesystem_error{filepath, errno};
}

File::~File() { close(m_fd); }

int File::fd() const { return m_fd; }

off64_t File::read(char* buf, size_t sz) { return ::read(m_fd, buf, sz); }

ssize_t File::write(char* buf, size_t sz) { return ::write(m_fd, buf, sz); }

void File::seek(off64_t offset) { lseek64(m_fd, offset, SEEK_SET); }

off64_t File::tell() const { return lseek64(m_fd, 0, SEEK_CUR); }

} // namespace hawk
