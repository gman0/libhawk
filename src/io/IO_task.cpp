#include "io/IO_task.h"

namespace hawk {

IO_task::IO_task(const std::vector<Path>& srcs, const Path& dst)
    : m_dst{dst}, m_total_size{0}, m_st{Status::not_started}
{
  m_sources = srcs;
  for (const Path& src : srcs)
    m_ts.emplace(src, dst / src.filename());
}

uintmax_t IO_task::total_size() const { return m_total_size; }

uintmax_t IO_task::total_offset() const { return m_total_offset; }

IO_task::Status IO_task::get_status() const { return m_st; }

void IO_task::start()
{
  if (m_st == Status::canceled)
    return;

  if (!m_ctx)
    m_ctx = std::make_unique<Context>(m_ts.back(), this);

  if (m_thread.joinable())
    m_thread.join();

  m_thread = Interruptible_thread([this] {
    if (!m_ctx->prepare_passed) {
      set_status(Status::preparing);
      try {
        prepare();
      }
      catch (const Filesystem_error& e) {
        handle_prepare_error(e);
        set_status(Status::failed);

        return;
      }

      m_ctx->prepare_passed = true;
    }

    set_status(Status::pending);
    try {
      const auto now = std::chrono::steady_clock::now();
      m_ctx->start = now;
      m_ctx->last_file_progress = now;

      start_tasking();
    }
    catch (const Filesystem_error& e) {
      handle_error(e, m_ts.front());
      set_status(Status::failed);

      return;
    }

    set_status(Status::finished);
  });
}

void IO_task::pause()
{
  m_thread.hard_interrupt();
  m_thread.join();
  set_status(Status::paused);
}

void IO_task::cancel()
{
  if (m_thread.joinable()) {
    m_thread.hard_interrupt();
    m_thread.join();
  }

  set_status(Status::canceled);
}

IO_task::Context* IO_task::context() const { return m_ctx.get(); }

void IO_task::set_status(IO_task::Status s)
{
  m_st = s;
  on_status_change(s);
}

void IO_task::Context::notify_task_progress(const IO_task::Total_progress& tp)
{
  m_parent->on_task_progress(tp);
}

void IO_task::Context::notify_file_progress(uintmax_t bytes_left,
                                            uintmax_t bytes_read)
{
  using namespace std::chrono;

  const auto now = steady_clock::now();
  if (now > last_file_progress + seconds{1}) {
    auto time_elapsed = duration_cast<seconds>(now - start);
    if (time_elapsed.count() == 0)
      return;

    uintmax_t rate = file_offset / time_elapsed.count();
    seconds eta = (bytes_left - bytes_read) * time_elapsed / bytes_read;
    m_parent->on_file_progress(
        File_progress{bytes_left, file_offset, rate, eta});

    last_file_progress = steady_clock::now();
  }
}

} // namespace hawk
