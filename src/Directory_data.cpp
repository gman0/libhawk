/*
 * Copyright (C) 2017 Róbert Vašek <gman@codefreax.org>
 *
 * This file is part of libhawk.
 *
 * libhawk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * libhawk is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libhawk.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Directory_data.h"
#include "Filesystem.h"
#include "Interrupt.h"
#include <cassert>

namespace hawk {

namespace {

inline bool run_filters(const std::vector<Directory_data::Filter>& fs,
                        const dirent64* ent)
{
  return std::all_of(fs.begin(), fs.end(),
                     [&](const auto& f) { return f(ent); });
}

void handle_lnk(dirent64* ent, const Path& base)
{
  int err;
  const Stat st = status(base / ent->d_name, err);

  if (err != 0)
    ent->d_type = DT_LNK_BROKEN_MASK;
  else
    ent->d_type = IFTODT(st.st_mode) | DT_LNK_MASK;
}

void handle_unknown(dirent64* ent, const Path& base)
{
  const unsigned char type = IFTODT(symlink_status(base / ent->d_name).st_mode);
  if (type == DT_LNK)
    handle_lnk(ent, base);
  else
    ent->d_type = type;
}

void handle_type(dirent64* ent, const Path& base)
{
  if (ent->d_type == DT_LNK)
    handle_lnk(ent, base);
  else if (ent->d_type == DT_UNKNOWN)
    handle_unknown(ent, base);
}

} // unnamed-namespace

void Directory_data::set_path(const Path& dir, int page, const Filters& fs)
{
  clear();

  iter = Directory_iterator{dir};

  if (!iter.at_end()) {
    iter.advance(page * pg_size);
    if (iter.at_end())
      iter = Directory_iterator{dir};
  }
  else
    return;

  uint64_t n = 0;

  for (; !iter.at_end() /* && n < pg_size */; ++iter) {
    soft_interruption_point();

    dirent64* ent = iter.entry();
    if (run_filters(fs, ent)) {
      if (ent->d_type == DT_UNKNOWN || ent->d_type == DT_LNK)
        handle_type(ent, dir);

      data.emplace_back();
      dirent64* dst = &data.back();
      memcpy(dst, ent, ent->d_reclen);

      ++n;
    }
  }

  for (uint64_t index = 0; index < n; index++)
    idx.push_back(index);
}

void Directory_data::clear()
{
  sort_id = 0;

  idx.clear();
  data.clear();
  iter = Directory_iterator{};
}

} // namespace hawk
